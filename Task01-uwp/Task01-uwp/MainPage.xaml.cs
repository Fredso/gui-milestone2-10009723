﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task01_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string selectedRadio = "";

        /******************** UI LOGIC ********************/
        public MainPage()
        {
            this.InitializeComponent();
            textBlock.Text = q1;
            textBlock1.Text = q2;
            textBlock2.Text = q3;
            textBlock3.Text = q4;
            highPriority.Content = rb1;
            medPriority.Content = rb2;
            lowPriority.Content = rb3;
            submitMessage.Content = cb1;
            clearAll.Content = cb2;
            toDepartment.ItemsSource = departments();
            medPriority.IsChecked=true;
        }
        //Radio Button code
         private void HandleRB(object sender, RoutedEventArgs e)
        {
            RadioButton priorityRB = sender as RadioButton;
            selectedRadio= priorityRB.Content.ToString();
        }
        //It clears the form
        private void clearForm()
        {
            toDepartment.SelectedIndex = -1;
            senderEmail.Text = "";
            subject.Text = "";
            medPriority.IsChecked = true;
            message.Text = "";
            instructions.Text = "";
        }
       //Submit Message Button. It calls a method that checks if all data is right
       private void submitMessage_Click(object sender, RoutedEventArgs e)
        {
            checkAllFields(senderEmail.Text, toDepartment.SelectedValue, subject.Text, message.Text);
        }
        //Dialog 1 showing the email summary
        private async void showSummaryDialog()
        {
            var dialog = new MessageDialog("Sender: " + senderEmail.Text + "\n" + "To: " + toDepartment.SelectedValue.ToString() + "\n" + "Subject: " + subject.Text +
                "\n" + "Priority: " + selectedRadio + "\n" + "Message: " + message.Text, "New Message");
            dialog.Commands.Add(new UICommand { Label = "Send to " + toDepartment.SelectedValue.ToString(), Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            dialog.Commands.Add(new UICommand { Label = "Reset", Id = 2 });
            var dialogAction = await dialog.ShowAsync();
            dialog.DefaultCommandIndex = 1;
            dialog.CancelCommandIndex = 1;
            takeActionsFromDialog1Selection((int)dialogAction.Id);         
      }
        //Dialog 2 showing to the user that the message was sent
        private async void showResultDialog()
        {
            var dialog2 = new MessageDialog(info1);
            dialog2.Commands.Add(new UICommand { Label = "OK" });
            var dialog2Action = await dialog2.ShowAsync();
        }
        //Dialog 3 asking the user to fill in all fields
        private async void showAlertDialog()
        {
            var dialog3 = new MessageDialog(info2);
            dialog3.Commands.Add(new UICommand { Label = "OK" });
            var dialog3Action = await dialog3.ShowAsync();
        }
        //Menu Button. It clears the form
        private void clearAll_Click(object sender, RoutedEventArgs e)
        {
            clearForm();
        }
        //It shows instructions in the notices area of the form
        private void senderEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            instructions.Text = i1;
        }
        //It shows instructions in the notices area of the form
        private void toDepartment_GotFocus(object sender, RoutedEventArgs e)
        {
            instructions.Text = i2;
        }
        //It shows instructions in the notices area of the form
        private void subject_GotFocus(object sender, RoutedEventArgs e)
        {
            instructions.Text = i3;
        }
        //It shows instructions in the notices area of the form
        private void message_GotFocus(object sender, RoutedEventArgs e)
        {
            instructions.Text = i4;
        }
        //It shows instructions in the notices area of the form
        private void highPriority_GotFocus(object sender, RoutedEventArgs e)
        {
            instructions.Text = i5;
        }
        //It shows instructions in the notices area of the form
        private void medPriority_GotFocus(object sender, RoutedEventArgs e)
        {
            instructions.Text = i5;
        }
        //It shows instructions in the notices area of the form
        private void lowPriority_GotFocus(object sender, RoutedEventArgs e)
        {
            instructions.Text = i5;
        }

        /******************** PROGRAM LOGIC ********************/
        //Laabels
        static string q1 = "To Department:";
        static string q2 = "Sender of email:";
        static string q3 = "Subject:";
        static string q4 = "Priority:";
        static string rb1 = "High";
        static string rb2 = "Medium";
        static string rb3 = "Low";
        static string cb1 = "Submit";
        static string cb2 = "Clear Form";
        static string i1 = "Please introduce the name of the email Sender";
        static string i2 = "Please introduce the targeted Department";
        static string i3 = "Please introduce the Subject of the message";
        static string i4 = "Please introduce the Message";
        static string i5 = "Please select the Priority of the message";
        static string info1 = "Your message was sent";
        static string info2 = "Please fill in all message fields";
        //It populates the departments list
        static List<string> departments()
        {
            var _myList = new List<string>();
            _myList.Add("Purchase");
            _myList.Add("Sales");
            _myList.Add("Admin");
            _myList.Add("Inventory");
            _myList.Add("HR");
            _myList.Add("Engineering");
            _myList.Add("Production");
            return _myList;
        }
        //It checks if all the fields have been filled in and launches the Dialog 1 with the email summary or Dialog 2 with an alert
        private void checkAllFields(string _sender, object _depSelection, string _subject, string _message)
        {
            if (_sender == "" || _depSelection == null || _subject == "" || _message == "") showAlertDialog();
            else showSummaryDialog();
        }
        //It acts depending on Dialog 1 selection
        private void takeActionsFromDialog1Selection(int dialog1Selection)
        { 
            switch (dialog1Selection)
            {
                case 0:
                    showResultDialog();
                    clearForm();
                    break;
                case 1:
                    break;
                case 2:
                    clearForm();
                    break;
            }
        }
    }
}
